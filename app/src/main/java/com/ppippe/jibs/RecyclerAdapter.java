package com.ppippe.jibs;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ppippe.jibs.model.VideoContents;
import com.ppippe.jibs.util.Log;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<VideoContents.Video> mData;
    private Context mContext;
    private OnRecyclerListener mListener;
    @LayoutRes
    private final int mLayoutResourceId;

    public RecyclerAdapter(Context context, int layoutResourceId, OnRecyclerListener listener) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mData = new ArrayList<>();
        mListener = listener;
        mLayoutResourceId = layoutResourceId;
    }

    public void setItems(List<VideoContents.Video> videos) {
        mData.clear();
        mData.addAll(videos);
        notifyDataSetChanged();
    }

    public List<VideoContents.Video> getItems() {
        return new ArrayList<>(mData);
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(mInflater.inflate(mLayoutResourceId, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder vh, final int position) {
        VideoContents.Video video = mData.get(position);
        vh.videoTitle.setText(video.title);
        Log.d("TAG", ":" + video.thumbnail);
        Picasso.with(mContext)
                .load(video.thumbnail)
                .placeholder(R.mipmap.video_thumbnail_default)
                .into(vh.videoThumbnail);
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRecyclerClicked(v, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView videoThumbnail;
        TextView videoTitle;


        public ViewHolder(View itemView) {
            super(itemView);
            videoThumbnail = (ImageView) itemView.findViewById(R.id.image_view);
            videoTitle = (TextView) itemView.findViewById(R.id.text_view);
        }
    }

}
