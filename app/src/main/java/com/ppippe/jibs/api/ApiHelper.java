package com.ppippe.jibs.api;


import com.ppippe.jibs.model.VideoContents;
import com.ppippe.jibs.util.Log;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public final class ApiHelper {
    private static final String TAG = ApiHelper.class.getSimpleName();
    private static String BASE_URL = "https://raw.githubusercontent.com/yun-github-public/CDN/master/";

    private ApiHelper() {
    }

    private static StubApi api() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(StubApi.class);
    }

    public static Observable<VideoContents> requestVideoContants() {
        return Observable.create(new Observable.OnSubscribe<VideoContents>() {
            @Override
            public void call(final Subscriber<? super VideoContents> subscriber) {
                api().getVideos()
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<VideoContents>() {
                            @Override
                            public void onStart() {
                                Log.d("TAG", "onStart");
                                subscriber.onStart();
                            }

                            @Override
                            public void onCompleted() {
                                Log.d("TAG", "onCompleted");
                                subscriber.onCompleted();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d("TAG", "onError" + e);
                                subscriber.onError(e);
                            }

                            @Override
                            public void onNext(VideoContents videoContents) {
                                if (videoContents.videos == null || videoContents.category == null)
                                    return;
                                subscriber.onNext(videoContents);
                            }
                        });
            }
        });
    }
}
