package com.ppippe.jibs.api;

import com.ppippe.jibs.model.VideoContents;

import retrofit2.http.GET;
import rx.Observable;

interface StubApi {
    @GET("jsons/1.json")
    Observable<VideoContents> getVideos();
}
