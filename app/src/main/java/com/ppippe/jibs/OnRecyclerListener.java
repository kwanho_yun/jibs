package com.ppippe.jibs;

import android.view.View;

public interface OnRecyclerListener {
    void onRecyclerClicked(View v, int position);
}
