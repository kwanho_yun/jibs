package com.ppippe.jibs.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import com.ppippe.jibs.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.widget.MediaController;

import java.io.IOException;

public final class VideoPlayerView extends TextureView implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnVideoSizeChangedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnInfoListener,
        TextureView.SurfaceTextureListener,
        MediaController.MediaPlayerControl, MediaPlayer.OnSeekCompleteListener {
    private static final String TAG = VideoPlayerView.class.getSimpleName();
    private MediaPlayer mMediaPlayer;
    private int mVideoWidth;
    private int mVideoHeight;
    private PlayStatus mPlayState;
    private int mBufferingPercent;
    private Uri mVideoUri;


    public enum PlayStatus {
        Idle,
        Released,
        Error,
        Initialized,
        Preparing,
        Prepared,
        Started,
        Paused,
        Stopped,
        PlaybackCompleted
    }

    public interface OnPlayerStatusChangeListener {
        void onPlayStatus(PlayStatus status);
    }

    private OnPlayerStatusChangeListener mOnPlayerStatusChangeListener;

    public VideoPlayerView(Context context) {
        super(context);
        init();
    }

    public VideoPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VideoPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VideoPlayerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
//        setBackgroundColor(Color.BLACK);
        changePlayStatus(PlayStatus.Idle);
        setSurfaceTextureListener(this);
        initMediaPlayer();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("TAG", "VideoPlayerView.onTouchEvent()");
        return super.onTouchEvent(event);
    }

    public void setOnPlayerStatusChangeListener(OnPlayerStatusChangeListener l) {
        mOnPlayerStatusChangeListener = l;
    }

    public int getVideoWidth() {
        return mVideoWidth;
    }

    public int getVideoHeight() {
        return mVideoHeight;
    }

    private void changePlayStatus(PlayStatus status) {
        mPlayState = status;
        OnPlayerStatusChangeListener l = mOnPlayerStatusChangeListener;
        if (l != null) {
            l.onPlayStatus(status);
        }
    }

    private boolean isPlayStatus(PlayStatus status) {
        return mPlayState.equals(status);
    }

    private boolean hasPlayStatus(PlayStatus... status) {
        for (PlayStatus state : status) {
            if (isPlayStatus(state)) return true;
        }
        return false;
    }

    private void initMediaPlayer() {
        if (mMediaPlayer != null) return;

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnBufferingUpdateListener(VideoPlayerView.this);
        mMediaPlayer.setOnCompletionListener(VideoPlayerView.this);
        mMediaPlayer.setOnPreparedListener(VideoPlayerView.this);
        mMediaPlayer.setOnVideoSizeChangedListener(VideoPlayerView.this);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnErrorListener(VideoPlayerView.this);
        mMediaPlayer.setOnInfoListener(VideoPlayerView.this);
        mMediaPlayer.setOnSeekCompleteListener(VideoPlayerView.this);
        mMediaPlayer.setScreenOnWhilePlaying(true);
    }

    public void playIfNeeded(Uri videoUri) {
        // TODO test
//        if (videoUri == null) return;
//        if (videoUri.equals(mVideoUri)
//                && hasPlayStatus(PlayStatus.Preparing,
//                PlayStatus.Started,
//                PlayStatus.Paused,
//                PlayStatus.PlaybackCompleted)) {
//            return;
//        }
        playWithVideoUri(videoUri);
    }

    private void playWithVideoUri(Uri videoUri) {
        initMediaPlayer();
        try {
            mVideoUri = videoUri;
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(getContext(), videoUri);
            changePlayStatus(PlayStatus.Initialized);
            preparAsync();
        } catch (IOException e) {
            onError(mMediaPlayer, 0, 0);
        }
    }

    private void preparAsync() {
        if (isPlayStatus(PlayStatus.Initialized)) {
            mBufferingPercent = 0;
            mMediaPlayer.prepareAsync();
            changePlayStatus(PlayStatus.Preparing);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        resize(widthMeasureSpec, heightMeasureSpec);
    }

    private void resize(int widthMeasureSpec, int heightMeasureSpec) {
        int vw = mVideoWidth;
        int vh = mVideoHeight;
        int width = getDefaultSize(vw, widthMeasureSpec);
        int height = getDefaultSize(vh, heightMeasureSpec);
        if (vw > 0 && vh > 0) {
            if (vw * height > width * vh) {
                height = width * vh / vw;
            } else if (vw * height < width * vh) {
                width = height * vw / vh;
            }
        }
        setMeasuredDimension(width, height);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        changePlayStatus(PlayStatus.PlaybackCompleted);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        mBufferingPercent = percent;
        Log.d("TAG3", "onBufferingUpdate()" + percent);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        changePlayStatus(PlayStatus.Prepared);
        start();
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        Log.d("TAG", "onVideoSizeChanged()" + width + "," + height);
        mVideoWidth = width;
        mVideoHeight = height;
        requestLayout();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        changePlayStatus(PlayStatus.Error);
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }

    /*

     */

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.d("TAG2", "onSurfaceTextureAvailable()");
        initMediaPlayer();
        if (mMediaPlayer != null) {
            mMediaPlayer.setSurface(new Surface(surface));
            if (canSeekTo()) {
                seekTo(mMediaPlayer.getCurrentPosition());
            }
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.d("TAG2", "onSurfaceTextureSizeChanged()");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.d("TAG2", "onSurfaceTextureDestroyed()");
        mMediaPlayer.setSurface(null);
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    private void stop() {
        if (hasPlayStatus(PlayStatus.Prepared,
                PlayStatus.Started,
                PlayStatus.Paused,
                PlayStatus.Stopped,
                PlayStatus.PlaybackCompleted)) {
            mMediaPlayer.stop();
            changePlayStatus(PlayStatus.Stopped);
        }
    }

    public void release() {
        if (!hasPlayStatus(PlayStatus.Idle, PlayStatus.Released)) {
            mMediaPlayer.release();
            mMediaPlayer = null;
            changePlayStatus(PlayStatus.Released);
        }
    }

    public boolean canStart() {
        return hasPlayStatus(PlayStatus.Prepared,
                PlayStatus.Started,
                PlayStatus.Paused,
                PlayStatus.PlaybackCompleted);
    }

    public boolean canPause() {
        return hasPlayStatus(PlayStatus.Started,
                PlayStatus.Paused,
                PlayStatus.PlaybackCompleted);
    }

    public boolean canSeekTo() {
        return hasPlayStatus(PlayStatus.Prepared,
                PlayStatus.Started,
                PlayStatus.Paused,
                PlayStatus.PlaybackCompleted);
    }


    public void start() {
        if (canStart()) {
            mMediaPlayer.start();
            changePlayStatus(PlayStatus.Started);
        }
    }

    public void seekTo(int seek) {
        if (canSeekTo()) {
            mMediaPlayer.seekTo(seek);
        }
    }

    public void pause() {
        if (canPause()) {
            mMediaPlayer.pause();
            changePlayStatus(PlayStatus.Paused);
        }
    }


    public int getDuration() {
        if (hasPlayStatus(PlayStatus.Prepared,
                PlayStatus.Started,
                PlayStatus.Paused,
                PlayStatus.Stopped,
                PlayStatus.PlaybackCompleted)) {
            return mMediaPlayer.getDuration();
        }
        return 0;
    }

    public int getCurrentPosition() {
        if (hasPlayStatus(PlayStatus.Initialized,
                PlayStatus.Prepared,
                PlayStatus.Started,
                PlayStatus.Paused,
                PlayStatus.Stopped,
                PlayStatus.PlaybackCompleted)) {
            return mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public boolean isPlaying() {
        return isPlayStatus(PlayStatus.Started);
    }

    public boolean isPaused() {
        return isPlayStatus(PlayStatus.Paused);
    }

    public int getBufferPercentage() {
        return mBufferingPercent;
    }

    public boolean canSeekBackward() {
        int duration = getDuration();
        int current = getCurrentPosition();
        return duration > 0 && current < 1000;

    }

    public boolean canSeekForward() {
        int duration = getDuration();
        int current = getCurrentPosition();
        return duration > 0 && current + 1000 < duration;
    }

    public int getAudioSessionId() {
        if (!hasPlayStatus(PlayStatus.Idle, PlayStatus.Released)) {
            return mMediaPlayer.getAudioSessionId();
        }
        return 0;
    }
}
