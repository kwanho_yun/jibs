package com.ppippe.jibs.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.ppippe.jibs.R;

import butterknife.BindView;
import butterknife.ButterKnife;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public final class DraggableLayout extends RelativeLayout {

    private static final int MARGIN_RIGHT = 16;
    private static final int MARGIN_BOTTOM = 16;
    private ViewDragHelper mDragHelper;

    private int mTop;
    private float mVerticalDragRange;
    private float mOffsetLeft;
    private int mLeft;
    private boolean mDraggable;

    @BindView(R.id.video_player_layout) View mTopView;
    @BindView(R.id.current_video_list_view) View mBottomView;
    @BindView(R.id.video_controller_view) View mControllerView;
    @BindView(R.id.video_player_view) VideoPlayerView mVideoPlayerView;
    @BindView(R.id.blur_background) View mBlur;

    public DraggableLayout(Context context) {
        super(context);
    }

    public DraggableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DraggableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public DraggableLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init() {
        setDraggable(true);
        mDragHelper = ViewDragHelper.create(this, 1f, new ViewDragHelper.Callback() {

            public float mScaleWidth;

            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                mScaleWidth = child.getWidth();
                return mTopView == child;
            }

            @Override
            public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
                mTop = top;
                mLeft = left;

                float scale = (getMeasuredHeight() - top) / (getMeasuredHeight() * 1f);
                float vDragOffset = top / mVerticalDragRange;
                float hDragOffset = 0;
                if (isAlignParentBottom() && mOffsetLeft > 0) {
                    hDragOffset = Math.abs(left) / mOffsetLeft * 1f;
                }

                //Log.d("TAG", "alpha1=" + vDragOffset + ", alpha2=" + hDragOffset);
                mTopView.setScaleX(scale);
                mTopView.setScaleY(scale);
                mTopView.setAlpha(Math.max(0f, 1f - hDragOffset));
                mTopView.setPivotX(changedView.getWidth() - MARGIN_RIGHT);//右寄せ
                mTopView.setPivotY(changedView.getHeight() - MARGIN_BOTTOM);
                mBottomView.setAlpha(Math.max(0f, 1f - vDragOffset));
                mControllerView.setAlpha(Math.round(1f - vDragOffset));
                if (vDragOffset == 0) {
                    mControllerView.setVisibility(View.VISIBLE);
                } else {
                    mControllerView.setVisibility(View.GONE);
                }
                requestLayout();
            }

            @Override
            public void onViewDragStateChanged(int state) {
                if (state == ViewDragHelper.STATE_IDLE
                        && isAlignParentRight()
                        && isAlignParentBottom()) {
                    int[] location = new int[2];
                    mTopView.getLocationInWindow(location);
                    mOffsetLeft = location[0];
                }

                if (state == ViewDragHelper.STATE_IDLE) {
                    //left out -> video pause
                    if (mTopView.getAlpha() == 0) {
                        mVideoPlayerView.pause();
                    }
                }
            }


            @Override
            public void onViewReleased(View child, float xvel, float yvel) {
                if (isAlignParentBottom()) {
                    if (mTopView.getAlpha() < 0.5f) {
                        int[] location = new int[2];
                        mTopView.getLocationInWindow(location);
                        mLeft -= location[0];
                        mDragHelper.settleCapturedViewAt(mLeft, mTopView.getTop());
                    } else {
                        mLeft = 0;
                        if (yvel < -5) {
                            mDragHelper.settleCapturedViewAt(mLeft, 0);
                        } else {
                            mDragHelper.settleCapturedViewAt(mLeft, mTopView.getTop());
                        }
                    }
                } else {
                    mLeft = 0;
                    if (yvel > 5) {
                        mDragHelper.settleCapturedViewAt(mLeft, getHeight() - child.getHeight());
                    } else {
                        mDragHelper.settleCapturedViewAt(mLeft, 0);
                    }
                }
                invalidate();
            }

            @Override
            public int getViewVerticalDragRange(View child) {
                return (int) mVerticalDragRange;
            }

            @Override
            public int clampViewPositionVertical(View child, int top, int dy) {
                if (isAlignParentRight()) {
                    final int topBound = getPaddingTop();
                    final int bottomBound = getHeight() - mTopView.getHeight() - mTopView.getPaddingBottom();
                    return Math.min(Math.max(top, topBound), bottomBound);
                }
                return mTopView.getTop();
            }

            @Override
            public int clampViewPositionHorizontal(View child, int left, int dx) {
                if (isAlignParentBottom()) {
                    return left;
                }
                final int leftBound = getPaddingLeft();
                final int rightBound = getWidth() - mTopView.getWidth() - mTopView.getPaddingRight();
                return Math.min(Math.max(left, leftBound), rightBound);
            }
        });
    }

    public void setDraggable(boolean draggable) {
        mDraggable = draggable;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        init();
    }

    @Override
    public void computeScroll() {
        if (mDragHelper.continueSettling(true)) {
            postInvalidateOnAnimation();
        }
    }

    public void slideUp() {
        if (mDragHelper.smoothSlideViewTo(mTopView, 0, 0)) {
            postInvalidateOnAnimation();
        }
    }

    public void slideDown() {
        if (mDragHelper.smoothSlideViewTo(mTopView, 0, getMeasuredHeight() - mTopView.getMeasuredHeight())) {
            postInvalidateOnAnimation();
        }
    }

    public void slideLeftOut() {
        int[] location = new int[2];
        mTopView.getLocationInWindow(location);
        if (mDragHelper.smoothSlideViewTo(mTopView, -location[0], mTop)) {
            postInvalidateOnAnimation();
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!mDraggable) {
            return super.onInterceptTouchEvent(event);
        }
        final int action = event.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mDragHelper.cancel();
                return false;
            default:
                break;
        }
        return mDragHelper.shouldInterceptTouchEvent(event);
    }
    /*
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        Log.d("TAG", "DraggableLayout.onInterceptTouchEvent()");
        final int action = event.getActionMasked();

        if (action != MotionEvent.ACTION_DOWN) {
            mDragHelper.cancel();
            return super.onInterceptTouchEvent(event);
        }

        final float x = event.getX();
        final float y = event.getY();
        boolean isHeaderViewUnder = false;

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                isHeaderViewUnder = mDragHelper.isViewUnder(mTopView, (int) x, (int) y);
                break;
            }
        }

        return mDragHelper.shouldInterceptTouchEvent(event) || isHeaderViewUnder;
    }
    */

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!mDraggable) {
            return super.onTouchEvent(event);
        }
        mDragHelper.processTouchEvent(event);

        final int x = (int) event.getX();
        final int y = (int) event.getY();

        boolean isHeaderViewUnder = mDragHelper.isViewUnder(mTopView, x, y);
        return isHeaderViewUnder && isViewHit(mTopView, x, y) || isViewHit(mBottomView, x, y);
    }

    private boolean isViewHit(View view, int x, int y) {
        int[] parentLocation = new int[2];
        int[] viewLocation = new int[2];

        getLocationOnScreen(parentLocation);
        view.getLocationOnScreen(viewLocation);
        int screenX = parentLocation[0] + x;
        int screenY = parentLocation[1] + y;
        return screenX >= viewLocation[0] && screenX < viewLocation[0] + view.getWidth() &&
                screenY >= viewLocation[1] && screenY < viewLocation[1] + view.getHeight();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        int maxHeight = MeasureSpec.getSize(heightMeasureSpec);
        setMeasuredDimension(resolveSizeAndState(maxWidth, widthMeasureSpec, 0),
                resolveSizeAndState(maxHeight, heightMeasureSpec, 0));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        mVerticalDragRange = getHeight() - mTopView.getHeight();
        mTopView.layout(mLeft, mTop, mLeft + r, mTop + mTopView.getMeasuredHeight());
        mBottomView.layout(l, mTop + mTopView.getMeasuredHeight(), r, mTop + b);
        mBlur.layout(l, t, r, b);
    }

    public boolean isAlignParentRight() {
        return mTopView.getRight() == getWidth();
    }

    public boolean isAlignParentBottom() {
        return mTopView.getBottom() == getHeight();
    }
}
