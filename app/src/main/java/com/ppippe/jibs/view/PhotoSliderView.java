package com.ppippe.jibs.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.ppippe.jibs.R;

public final class PhotoSliderView extends BaseSliderView {
    public PhotoSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.item_image_slider, null);
        ImageView target = (ImageView) v.findViewById(R.id.slider_image);
        bindEventAndShow(v, target);
        return v;
    }
}
