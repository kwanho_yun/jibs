package com.ppippe.jibs.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ppippe.jibs.R;
import com.ppippe.jibs.util.AnimationFactory;
import com.ppippe.jibs.util.DateUtils;
import com.ppippe.jibs.util.Log;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public final class VideoPlayerLayout extends RelativeLayout implements VideoPlayerView.OnPlayerStatusChangeListener {

    @BindView(R.id.btn_play) View mPlayBtn;
    @BindView(R.id.btn_pause) View mPauseBtn;
    @BindView(R.id.btn_full_screen) View mFullScreenBtn;
    @BindView(R.id.btn_half_screen) View mHalfScreenBtn;
    @BindView(R.id.btn_minimize_screen) View mMinimizeScreenBtn;
    @BindView(R.id.video_player_view) VideoPlayerView mVideoPlayerView;
    @BindView(R.id.seekbar) SeekBar mSeekbar;
    @BindView(R.id.progressbar) ViewGroup mProgressbar;
    @BindView(R.id.video_controller_view) View mVideoControllerView;
    @BindView(R.id.total_time) TextView mTotalTime;
    @BindView(R.id.current_time) TextView mCurrentTime;

    public VideoPlayerLayout(Context context) {
        super(context);
    }

    public VideoPlayerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoPlayerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VideoPlayerLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        hidePauseButton();
        hidePlayButton();
        hideProgressBar();
        hideSeekBar();
        hideControllerView();
        mVideoPlayerView.setOnPlayerStatusChangeListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            showControllerView(false);
        }
        return super.onTouchEvent(event);
    }

    private void showSeekBar() {
        mSeekbar.setClickable(true);
        mSeekbar.setVisibility(View.VISIBLE);
        showControllerView(false);
    }

    private void hideSeekBar() {
        mSeekbar.setClickable(false);
        mSeekbar.setVisibility(View.GONE);
    }

    private void showPlayButton() {
        mPlayBtn.setClickable(true);
        mPlayBtn.setVisibility(View.VISIBLE);
        showControllerView(true);
    }

    private void hidePlayButton() {
        mPlayBtn.setClickable(false);
        mPlayBtn.setVisibility(View.GONE);
    }

    private void showPauseButton() {
        mPauseBtn.setClickable(true);
        mPauseBtn.setVisibility(View.VISIBLE);
        showControllerView(false);
    }

    private void hidePauseButton() {
        mPauseBtn.setClickable(false);
        mPauseBtn.setVisibility(View.GONE);
    }

    private void showProgressBar() {
        mProgressbar.setVisibility(View.VISIBLE);
        ((AVLoadingIndicatorView) mProgressbar.getChildAt(0)).show();
        showControllerView(false);
    }

    private void hideProgressBar() {
        ((AVLoadingIndicatorView) mProgressbar.getChildAt(0)).hide();
        mProgressbar.setVisibility(View.GONE);
    }

    public VideoPlayerView getVideoPlayerView() {
        return mVideoPlayerView;
    }

    public void showVideoError() {
        Crouton.makeText((Activity) getContext(), "Load error...", Style.ALERT).show();
    }

    private Runnable mHideControllerViewTask = new Runnable() {
        @Override
        public void run() {
            hideControllerView();
        }
    };

    private Runnable mSeekBarUpdateTask = new Runnable() {
        @Override
        public void run() {
            // TODO buffering
            int current = mVideoPlayerView.getCurrentPosition();
            Log.d("TAG", "seek:" + current);
            updateSeekBar(current, current);
            postDelayed(this, 100);
        }
    };

    public void showControllerView(boolean isKeep) {
        mFullScreenBtn.setEnabled(true);
        mHalfScreenBtn.setEnabled(true);
        mMinimizeScreenBtn.setEnabled(true);

        removeCallbacks(mHideControllerViewTask);
        if (!isKeep) {
            postDelayed(mHideControllerViewTask, 2000);
        }
        if (mVideoControllerView.getAlpha() < 1f) {
            ObjectAnimator anim = AnimationFactory.fadeIn(mVideoControllerView);
            anim.setDuration(500);
            anim.start();
        }
    }

    public void hideControllerView() {
        ObjectAnimator anim = AnimationFactory.fadeOut(mVideoControllerView);
        anim.setDuration(500);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mFullScreenBtn.setEnabled(false);
                mHalfScreenBtn.setEnabled(false);
                mMinimizeScreenBtn.setEnabled(false);
            }
        });
        anim.start();
    }

    void initSeekBar(int progress, int max) {
        mTotalTime.setText(DateUtils.conertToPlayTimeFormat(max));
        mCurrentTime.setText(DateUtils.conertToPlayTimeFormat(0));
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mVideoPlayerView.seekTo(progress);
                    updateCurrentTime(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                showControllerView(true);
                removeCallbacks(mSeekBarUpdateTask);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                showControllerView(false);
                post(mSeekBarUpdateTask);
            }
        });
        mSeekbar.setMax(max);
        mSeekbar.setSecondaryProgress(progress);
        mSeekbar.setProgress(progress);
    }

    void updateSeekBar(int progress, int secondaryProgrress) {
        mSeekbar.setSecondaryProgress(secondaryProgrress);
        mSeekbar.setProgress(progress);
        updateCurrentTime(progress);
    }

    void updateCurrentTime(int progress) {
        mCurrentTime.setText(DateUtils.conertToPlayTimeFormat(progress));
    }

    @Override
    public void onPlayStatus(VideoPlayerView.PlayStatus status) {
        Log.d("TAG", "onPlayStatus()" + status);
        removeCallbacks(mSeekBarUpdateTask);
        switch (status) {
            case Idle:
                break;
            case Released:
                break;
            case Error:
                showVideoError();
                hidePlayButton();
                hidePauseButton();
                break;
            case Initialized:
                hidePlayButton();
                hidePauseButton();
                showProgressBar();
                break;
            case Preparing:
                break;
            case Prepared:
                initSeekBar(mVideoPlayerView.getCurrentPosition(), mVideoPlayerView.getDuration());
                hideProgressBar();
                showSeekBar();
                break;
            case Started:
                post(mSeekBarUpdateTask);
                showPauseButton();
                hidePlayButton();
                break;
            case Paused:
                showPlayButton();
                hidePauseButton();
                break;
            case Stopped:
                break;
            case PlaybackCompleted:
                showPlayButton();
                hidePauseButton();
                break;
        }
    }
}
