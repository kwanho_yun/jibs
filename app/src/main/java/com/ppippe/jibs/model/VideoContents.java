package com.ppippe.jibs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoContents {
    @SerializedName("category")
    @Expose
    public Category category;

    @SerializedName("videos")
    @Expose
    public List<Video> videos;

    public static class Category {
        @SerializedName("id")
        @Expose
        public Long id;
        @SerializedName("name")
        @Expose
        public String name;

        public static class Page {
            @SerializedName("begin")
            @Expose
            public Long begin;
            @SerializedName("end")
            @Expose
            public Long end;
        }
    }

    public static class Video {
        @SerializedName("id")
        @Expose
        public Long id;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("uri")
        @Expose
        public String uri;
        @SerializedName("thumbnail")
        @Expose
        public String thumbnail;
    }
}

