package com.ppippe.jibs.util;

import com.ppippe.jibs.common.JibsApplication;

public final class Log {
    private static final boolean DEBUG = true;
    private static final String TAG = JibsApplication.class.getPackage().getName();

    public static final void v(String tag, String msg) {
        log(android.util.Log.VERBOSE, tag, msg, null);
    }

    public static final void d(String tag, String msg) {
        log(android.util.Log.DEBUG, tag, msg, null);
    }

    public static final void i(String tag, String msg) {
        log(android.util.Log.INFO, tag, msg, null);
    }

    public static final void w(String tag, String msg) {
        log(android.util.Log.WARN, tag, msg, null);
    }

    public static final void w(String tag, Throwable throwable) {
        log(android.util.Log.WARN, tag, "warn:", throwable);
    }

    public static final void e(String tag, String msg) {
        log(android.util.Log.ERROR, tag, msg, null);
    }

    public static final void e(String tag, Throwable throwable) {
        log(android.util.Log.ERROR, tag, "error:", throwable);
    }

    private static final void log(int level, String clazzName, String msg, Throwable throwable) {
        if (!DEBUG) {
            return;
        }
        if (msg == null) {
            msg = "null";
        }
        String tag = TAG + "." + clazzName;
        switch (level) {
            case android.util.Log.VERBOSE:
                android.util.Log.v(tag, msg);
                if (throwable != null) {
                    android.util.Log.v(tag, msg, throwable);
                } else {
                    android.util.Log.v(tag, msg);
                }
                break;
            case android.util.Log.DEBUG:
                if (throwable != null) {
                    android.util.Log.d(tag, msg, throwable);
                } else {
                    android.util.Log.d(tag, msg);
                }
                break;
            case android.util.Log.INFO:
                if (throwable != null) {
                    android.util.Log.i(tag, msg, throwable);
                } else {
                    android.util.Log.i(tag, msg);
                }
                break;
            case android.util.Log.WARN:
                if (throwable != null) {
                    android.util.Log.w(tag, msg, throwable);
                } else {
                    android.util.Log.w(tag, msg);
                }
                break;
            case android.util.Log.ERROR:
                if (throwable != null) {
                    android.util.Log.e(tag, msg, throwable);
                } else {
                    android.util.Log.e(tag, msg);
                }
                break;
        }
    }
}