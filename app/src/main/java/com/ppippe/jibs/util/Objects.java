package com.ppippe.jibs.util;

public final class Objects {
    private Objects() {
    }

    public static <T> T throwIfNull(T t) throws NullPointerException {
        if (t == null) throw new NullPointerException();
        return t;
    }
}
