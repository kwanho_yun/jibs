package com.ppippe.jibs.util;


public class TranstationCalulator {

    public static float translationY(float width, float height, float scale) {
        return (width - height) / 2 + ((scale - 1) * (width / 2));
    }

    public static float[] resizeTo(float width, float height, float destWidth, float destHeight) {
        Log.d("TAG", "resizeTo()" + width + ", " + height + ", " + destWidth + ", " + destHeight);
        float resizedWidth;
        float resizedHeight;
        if (width / height < destWidth / destHeight) {
            resizedHeight = height * destHeight / height;
            resizedWidth = width * destHeight / height;
            if (resizedWidth % 1 > 0) {
                resizedWidth = ((int) resizedWidth) + 1;
            }

        } else {
            resizedWidth = width * destWidth / width;
            resizedHeight = height * destWidth / width;
            if (resizedHeight % 1 > 0) {
                resizedHeight = ((int) resizedHeight) + 1;
            }
        }

        return new float[]{resizedWidth, resizedHeight};
    }
}
