package com.ppippe.jibs.util;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

public final class Toasts {
    private Toasts() {
    }

    public static void show(final Context context, final String msg) {
        if (Threads.isUiThread()) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    show(context, msg);
                }
            });
        }
    }
}
