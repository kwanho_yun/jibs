package com.ppippe.jibs.util;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;

public final class VideoFileManager {
    private static final String TAG = VideoFileManager.class.getSimpleName();
    private static final String VIDEO_TITLE = "robust.photoframe.demo.action.AdLoadService.VIDEO_TITLE";
    private static final String VIDEO_MIME_TYPE = "application/mp4";

    private VideoFileManager() {
    }

    public static void clearInvalidFiles(Context context) {
        Log.d(TAG, "clearInvalidFiles()");
        DownloadManager dmm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Cursor c = null;
        try {
            c = dmm.query(new DownloadManager.Query());
            if (c != null) {
                while (c.moveToNext()) {
                    long id = c.getLong(c.getColumnIndex(DownloadManager.COLUMN_ID));
                    String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
                    String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    String mimeType = c.getString(c.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
                    if (VIDEO_TITLE.equals(title)
                            && VIDEO_MIME_TYPE.equals(mimeType)) {
                        Log.d(TAG, "deleteOldFile:" + uriString);
                        dmm.remove(id);
                    }
                }
            }
        } finally {
            if (c != null) c.close();
        }
    }

    public static void download(Context context, Uri uri) {
        Log.d(TAG, "download:" + uri);
        DownloadManager dmm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());
        request.setTitle(VIDEO_TITLE);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setMimeType("application/mp4");
        //request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        dmm.enqueue(request);
    }

    public static Uri nextVideoUri(Context context) {
        Log.d(TAG, "nextVideoUri:");
        DownloadManager dmm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Cursor c = null;
        try {
            c = dmm.query(new DownloadManager.Query().setFilterByStatus(DownloadManager.STATUS_SUCCESSFUL));
            if (c != null) {
                while (c.moveToNext()) {
                    String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
                    String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    String mimeType = c.getString(c.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
                    if (VIDEO_TITLE.equals(title)
                            && VIDEO_MIME_TYPE.equals(mimeType)
                            && uriString != null) {
                        return Uri.parse(uriString);
                    }
                }
            }
        } finally {
            if (c != null) c.close();
        }
        return null;
    }

    public static void deleteWithVideoUri(Context context, Uri uri) {
        Log.d(TAG, "deleteWithVideoUri:" + uri);
        DownloadManager dmm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Cursor c = null;
        try {
            c = dmm.query(new DownloadManager.Query());
            if (c != null) {
                while (c.moveToNext()) {
                    long id = c.getLong(c.getColumnIndex(DownloadManager.COLUMN_ID));
                    String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
                    String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    String mimeType = c.getString(c.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
                    if (VIDEO_TITLE.equals(title)
                            && VIDEO_MIME_TYPE.equals(mimeType)
                            && uri.toString().equals(uriString)) {
                        Toasts.show(context, "delete file:" + uri);
                        int deletedId = dmm.remove(id);
                        android.util.Log.d(TAG, "delete video:" + deletedId);
                        return;
                    }
                }
            }
        } finally {
            if (c != null) c.close();
        }
    }
}
