package com.ppippe.jibs.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;

import com.google.firebase.crash.FirebaseCrash;
import com.ppippe.jibs.R;

public final class CustomTabs {
    private CustomTabs() {
    }

    public static void open(@NonNull Activity activity, @NonNull Uri uri) {
        Objects.throwIfNull(activity);
        Objects.throwIfNull(uri);

        try {
            new CustomTabsIntent.Builder()
                    .setShowTitle(true)
                    .setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary))
                    .build()
                    .launchUrl(activity, uri);
        } catch (ActivityNotFoundException e) {
            Toasts.show(activity, "Required Chrome Version 45 or later");
            FirebaseCrash.report(new Exception("Chrome Version Unsupported Exception"));
        }

    }
}
