package com.ppippe.jibs.util;

import android.os.Looper;

public final class Threads {
    private Threads() {
    }

    public static boolean isUiThread() {
        return Thread.currentThread().equals(Looper.getMainLooper().getThread());
    }
}
