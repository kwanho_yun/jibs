package com.ppippe.jibs.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class DateUtils {
    private DateUtils() {
    }

    public static String conertToPlayTimeFormat(long time) {
        long hour = TimeUnit.HOURS.toMillis(1);
        String pattern;
        if (time > hour) {
            pattern = "HH:mm:ss";
        } else {
            pattern = "mm:ss";
        }
        return new SimpleDateFormat(pattern).format(new Date(time));
    }
}
