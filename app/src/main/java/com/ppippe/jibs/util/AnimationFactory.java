package com.ppippe.jibs.util;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.support.annotation.NonNull;
import android.view.View;

public class AnimationFactory {
    public static ObjectAnimator maximumScreen(View view, float width, float height) {
        float[] scaleAndMoveY = scaleAndTranslationY(view, width, height);
        return rotationRight(view, scaleAndMoveY[0], scaleAndMoveY[1]);
    }

    public static ObjectAnimator minimumScreen(View view, float width, float height) {
        float[] scaleAndMoveY = scaleAndTranslationY(view, width, height);
        return rotationLeft(view, scaleAndMoveY[0], scaleAndMoveY[1]);
    }

    private static float[] scaleAndTranslationY(View view, float width, float height) {
        float[] wh = TranstationCalulator.resizeTo(view.getWidth(), view.getHeight(), width, height);
        float moveToY = 0;
        float scale = 0;
        if (width == wh[0] && height == wh[1]) {
            scale = wh[0] / view.getWidth();
            moveToY = TranstationCalulator.translationY(view.getWidth(), view.getHeight(), scale);
        } else if (width == wh[0]) {
            //move to (height - wh[1])/2
            scale = wh[0] / view.getWidth();
            moveToY = TranstationCalulator.translationY(view.getWidth(), view.getHeight(), scale);
        } else if (height == wh[1]) {
            //move to (width - wh[0])/2
            float y = (width - wh[0]) / 2;
            scale = wh[1] / view.getHeight();
            moveToY = TranstationCalulator.translationY(view.getWidth(), view.getHeight(), scale) + y;
        }
        return new float[]{moveToY, scale};
    }

    @NonNull
    private static ObjectAnimator rotationRight(View view, float moveToY, float scale) {
        PropertyValuesHolder pivotX = PropertyValuesHolder.ofFloat("pivotX", 540f);
        PropertyValuesHolder pivotY = PropertyValuesHolder.ofFloat("pivotY", 303.5f);
        PropertyValuesHolder scaleX = PropertyValuesHolder.ofFloat("scaleX", 1f, scale);
        PropertyValuesHolder scaleY = PropertyValuesHolder.ofFloat("scaleY", 1f, scale);
        PropertyValuesHolder translationY = PropertyValuesHolder.ofFloat("translationY", 0f, moveToY);
        PropertyValuesHolder rotationRight = PropertyValuesHolder.ofFloat("rotation", 0f, 90f);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view, pivotX, pivotY, scaleX, scaleY, translationY, rotationRight);
        return animator;
    }

    @NonNull
    private static ObjectAnimator rotationLeft(View view, float moveToY, float scale) {
        PropertyValuesHolder scaleX = PropertyValuesHolder.ofFloat("scaleX", scale, 1f);
        PropertyValuesHolder scaleY = PropertyValuesHolder.ofFloat("scaleY", scale, 1f);
        PropertyValuesHolder translationY = PropertyValuesHolder.ofFloat("translationY", moveToY, 0f);
        PropertyValuesHolder rotationRight = PropertyValuesHolder.ofFloat("rotation", 90f, 0f);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view, scaleX, scaleY, translationY, rotationRight);
        return animator;
    }

    public static ObjectAnimator fadeIn(View view) {
        PropertyValuesHolder fadeIn = PropertyValuesHolder.ofFloat("alpha", 0f, 1f);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view, fadeIn);
        return animator;
    }

    public static ObjectAnimator fadeOut(View view) {
        PropertyValuesHolder fadeOut = PropertyValuesHolder.ofFloat("alpha", 1f, 0f);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view, fadeOut);
        return animator;
    }
}
