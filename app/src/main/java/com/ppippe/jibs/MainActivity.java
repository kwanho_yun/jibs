package com.ppippe.jibs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.ppippe.jibs.api.ApiHelper;
import com.ppippe.jibs.common.JibsActivity;
import com.ppippe.jibs.util.AnimationFactory;
import com.ppippe.jibs.util.CustomTabs;
import com.ppippe.jibs.util.DisplayUtils;
import com.ppippe.jibs.model.VideoContents;
import com.ppippe.jibs.util.Log;
import com.ppippe.jibs.view.DraggableLayout;
import com.ppippe.jibs.view.PhotoSliderView;
import com.ppippe.jibs.view.VideoPlayerLayout;
import com.ppippe.jibs.view.VideoPlayerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observer;
import rx.Subscription;

public class MainActivity extends JibsActivity {
    private final static String APPLICATION_RAW_PATH = "android.resource://com.ppippe.jibs/";

    private static int STATUS_BAR_HEIGHT;
    private static int NAVIGATION_BAR_HEIGHT;
    private static int SCREEN_SIZE_WIDTH;
    private static int SCREEN_SIZE_HEIGHT;

    private RecyclerAdapter mVideoListAdapter1;
    private RecyclerAdapter mVideoListAdapter2;
    private RecyclerAdapter mVideoListAdapter3;
    private RecyclerAdapter mCurrentVideoListAdapter;
    private Status mStatus = Status.Main;
    // TODO add
    private Subscription mVideoList1Subscription;

    private enum Status {
        FullScreen,
        ToFullScreen,
        HalfScreen,
        ToHalfScreen,
        Main,
        ToMain,
    }

    @BindView(R.id.video_listview_1) RecyclerView mVideoListView1;
    @BindView(R.id.video_listview_2) RecyclerView mVideoListView2;
    @BindView(R.id.video_listview_3) RecyclerView mVideoListView3;
    @BindView(R.id.draggable_layout) DraggableLayout mDraggableLayout;
    @BindView(R.id.video_player_layout) VideoPlayerLayout mVideoPlayerLayout;
    @BindView(R.id.video_player_view) VideoPlayerView mVideoPlayerView;
    @BindView(R.id.current_video_list_view) RecyclerView mCurrentVideoListView;
    @BindView(R.id.photo_slider_view) SliderLayout mPhotoSliderView;
    @BindView(R.id.btn_full_screen) View mFullScreenBtn;
    @BindView(R.id.btn_half_screen) View mHalfScreenBtn;
    @BindView(R.id.btn_minimize_screen) View mMinimizeScreenBtn;
    @BindView(R.id.toolbar_layout) View mToobarLayout;
    @BindView(android.R.id.content) View mContentLayout;
    @BindView(R.id.blur_background) View mBlurBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initConstants();
        initToolBar();
        initPhotoSlider();
        initHotVideoList();
        initLatestVideoList();
        initJejuVideoList();
        initSelectVideoList();

        loadHotVideoList();
        loadLatestVideoList();
        loadJejuVideoList();
    }

    private void initConstants() {
        final int[] size = DisplayUtils.getScreenSize(this);
        SCREEN_SIZE_WIDTH = size[0];
        SCREEN_SIZE_HEIGHT = size[1];
        STATUS_BAR_HEIGHT = DisplayUtils.getStatusBarHeight(getResources());
        NAVIGATION_BAR_HEIGHT = DisplayUtils.getNavigationBarHeight(getResources());
    }

    private void initPhotoSlider() {
        HashMap<String, String> urls = new HashMap<String, String>();
        urls.put("A", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        urls.put("B", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        urls.put("C", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        urls.put("D", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");
        urls.put("E", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");
        urls.put("F", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        for (String name : urls.keySet()) {
            PhotoSliderView photoSlider = new PhotoSliderView(this);
            photoSlider
                    .bundle(new Bundle())
                    .image(urls.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            //Toast.makeText(MainActivity.this, slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
                            // TODO Vide Play
                            //playWithUri((Uri) null);
                            // TODO Open Url
                            CustomTabs.open(MainActivity.this, Uri.parse("http://www.google.com"));
                        }
                    });
            logEvent("Slider.click", "uri", name);
            photoSlider.getBundle().putString("extra", name);
            mPhotoSliderView.addSlider(photoSlider);
        }
        mPhotoSliderView.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mPhotoSliderView.setCustomAnimation(new DescriptionAnimation());
        mPhotoSliderView.setDuration(2000);
        PagerIndicator indicator = mPhotoSliderView.getPagerIndicator();
        indicator.setDefaultIndicatorColor(Color.parseColor("#FF9800"), Color.parseColor("#212121"));
        indicator.setDefaultIndicatorShape(PagerIndicator.Shape.Rectangle);
        indicator.setDefaultIndicatorSize(32, 2, PagerIndicator.Unit.DP);
        mPhotoSliderView.setCustomIndicator(indicator);
    }


    @Override
    protected void onPause() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("TAG", "Refreshed token: " + refreshedToken);

        mPhotoSliderView.stopAutoCycle();
        mVideoPlayerView.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVideoList1Subscription != null
                && !mVideoList1Subscription.isUnsubscribed()) {
            mVideoList1Subscription.unsubscribe();
        }
        mVideoPlayerView.release();
    }

    void playWithUri(Uri uri2) {
        // TODO
        logEvent("Video.play", "uri", "xxx");
//        Uri uri = Uri.parse("https://github.com/yun-github-public/CDN/raw/master/videos/a.mp4");
//        Uri uri = Uri.parse(APPLICATION_RAW_PATH + R.raw.video);
//        setState(State.VideoBuffering);
        slideUpIfNeeded();
        Uri uri = Uri.parse(APPLICATION_RAW_PATH + R.raw.jibs);
//        Uri uri = Uri.parse("http://stone-b.baka.co.jp/pub_data/20170701154838085TQvvw_200K.mp4");
        mDraggableLayout.slideUp();
        mVideoPlayerView.playIfNeeded(uri);
    }

    void initHotVideoList() {
        mVideoListAdapter1 = new RecyclerAdapter(this, R.layout.item_video_horizontal, new OnRecyclerListener() {
            @Override
            public void onRecyclerClicked(View v, int position) {
                Log.d("TAG", "onRecyclerClicked()");
                mCurrentVideoListAdapter.setItems(mVideoListAdapter1.getItems());
                playWithUri((Uri) null);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mVideoListView1.setAdapter(mVideoListAdapter1);
        mVideoListView1.setLayoutManager(layoutManager);
    }

    void initLatestVideoList() {
        mVideoListAdapter2 = new RecyclerAdapter(this, R.layout.item_video_horizontal, new OnRecyclerListener() {
            @Override
            public void onRecyclerClicked(View v, int position) {
                Log.d("TAG", "onRecyclerClicked()");
                mCurrentVideoListAdapter.setItems(mVideoListAdapter2.getItems());
                // TODO
                playWithUri((Uri) null);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mVideoListView2.setAdapter(mVideoListAdapter2);
        mVideoListView2.setLayoutManager(layoutManager);
    }

    void initJejuVideoList() {
        mVideoListAdapter3 = new RecyclerAdapter(this, R.layout.item_video_horizontal, new OnRecyclerListener() {
            @Override
            public void onRecyclerClicked(View v, int position) {
                Log.d("TAG", "onRecyclerClicked()");
                mCurrentVideoListAdapter.setItems(mVideoListAdapter3.getItems());
                // TODO
                playWithUri((Uri) null);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mVideoListView3.setAdapter(mVideoListAdapter3);
        mVideoListView3.setLayoutManager(layoutManager);
    }

    private void initSelectVideoList() {
        mCurrentVideoListAdapter = new RecyclerAdapter(this, R.layout.item_video_vertical, new OnRecyclerListener() {
            @Override
            public void onRecyclerClicked(View v, int position) {
                Log.d("TAG", "onRecyclerClicked()");
                // TODO
                playWithUri((Uri) null);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mCurrentVideoListView.setAdapter(mCurrentVideoListAdapter);
        mCurrentVideoListView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPhotoSliderView.startAutoCycle();
    }

    void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        Log.d("TAG", "onBackPressed()" + mStatus);
        switch (mStatus) {
            case FullScreen:
                changeHalfScreen();
                break;
            case HalfScreen:
                if (mDraggableLayout.isAlignParentBottom()) {
                    Log.d("TAG", "onBackPressed(1)");
                    if (mVideoPlayerLayout.getAlpha() == 0) {
                        super.onBackPressed();
                    } else {
                        mDraggableLayout.slideLeftOut();
                        mStatus = Status.Main;
                    }

                } else {
                    Log.d("TAG", "onBackPressed(2)");
                    slideDown();
                }
                break;
            case Main:
                super.onBackPressed();
                break;
            default:
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_menu:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @OnClick(R.id.btn_full_screen)
    public void onFullScreenBtnClick() {
        changeFullScreen();
    }

    @OnClick(R.id.btn_half_screen)
    public void onHalfScreenBtnClick() {
        changeHalfScreen();
    }

    @OnClick(R.id.btn_minimize_screen)
    public void onMinimizeScreenBtnClick() {
        mDraggableLayout.slideDown();
    }

    @OnClick(R.id.btn_play)
    public void onPlayBtnClick() {
        mVideoPlayerView.start();
    }

    @OnClick(R.id.btn_pause)
    public void onPauseBtnClick() {
        mVideoPlayerView.pause();
    }

    void changeHalfScreen() {
        mBlurBackground.setVisibility(View.GONE);
        mBlurBackground.setAnimation(null);

        mFullScreenBtn.setVisibility(View.GONE);
        mHalfScreenBtn.setVisibility(View.GONE);
        mMinimizeScreenBtn.setVisibility(View.GONE);

        mStatus = Status.ToHalfScreen;
        mDraggableLayout.setDraggable(true);

        showNavigationBar();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mContentLayout.getLayoutParams();
        Log.d("TAG", "slideUp(1)" + params.topMargin);
        ValueAnimator animator = ValueAnimator.ofInt(-STATUS_BAR_HEIGHT, 0, STATUS_BAR_HEIGHT);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.topMargin = (Integer) valueAnimator.getAnimatedValue();
                params.bottomMargin = NAVIGATION_BAR_HEIGHT;
                Log.d("TAG", "slideUp(2)" + params.topMargin);
                mContentLayout.requestLayout();
            }
        });
        animator.setDuration(300);
        animator.start();


        Animator anim = AnimationFactory.minimumScreen(mVideoPlayerLayout, SCREEN_SIZE_HEIGHT, SCREEN_SIZE_WIDTH);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mStatus = Status.HalfScreen;
                mFullScreenBtn.setVisibility(View.VISIBLE);
                mMinimizeScreenBtn.setVisibility(View.VISIBLE);
            }
        });
        anim.setDuration(300);
        anim.start();
    }

    void changeFullScreen() {
        mBlurBackground.setVisibility(View.VISIBLE);
        Animation a = new AlphaAnimation(0f, 1);
        a.setDuration(300);
        mBlurBackground.startAnimation(a);

        mFullScreenBtn.setVisibility(View.GONE);
        mHalfScreenBtn.setVisibility(View.GONE);
        mMinimizeScreenBtn.setVisibility(View.GONE);

        mStatus = Status.ToFullScreen;
        mDraggableLayout.setDraggable(false);

        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mContentLayout.getLayoutParams();
        Log.d("TAG", "changeFullScreen(1)" + params.topMargin);
        ValueAnimator animator = ValueAnimator.ofInt(0, -STATUS_BAR_HEIGHT, 0);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                params.topMargin = (Integer) valueAnimator.getAnimatedValue();
                params.bottomMargin = 0;
                Log.d("TAG", "changeFullScreen(2)" + params.topMargin);
                mContentLayout.requestLayout();
            }
        });
        animator.setDuration(300);
        animator.start();

        Animator anim = AnimationFactory.maximumScreen(mVideoPlayerLayout, SCREEN_SIZE_HEIGHT, SCREEN_SIZE_WIDTH);
        anim.setDuration(300);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mStatus = Status.FullScreen;
                mHalfScreenBtn.setVisibility(View.VISIBLE);

                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                hideNavigationBar();
            }
        });
        anim.start();
    }

    private void hideNavigationBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void showNavigationBar() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
    }

    void loadHotVideoList() {
        // TODO TEST
        List<VideoContents.Video> videoList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            VideoContents.Video v = new VideoContents.Video();
            v.id = Long.valueOf(i);
            v.thumbnail = "http://abc.com/a.png";
            v.uri = "";
            v.title = "title" + i;
            videoList.add(v);
        }

        mVideoListAdapter1.setItems(videoList);
        mVideoListAdapter2.setItems(videoList);
        mVideoListAdapter3.setItems(videoList);

        mVideoList1Subscription = ApiHelper.requestVideoContants()
                .subscribe(new Observer<VideoContents>() {
                    @Override
                    public void onCompleted() {
                        Log.d("TAG", "onCompleted()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("TAG", "onCompleted()" + e);
                    }

                    @Override
                    public void onNext(VideoContents videoContents) {
                        Log.d("TAG", "onCompleted()" + videoContents.videos.size());
                        List<VideoContents.Video> videoList = videoContents.videos;
                        mVideoListAdapter1.setItems(videoList);
                        mVideoListAdapter2.setItems(videoList);
                        mVideoListAdapter3.setItems(videoList);
                    }
                });
    }

    private void loadJejuVideoList() {
        // TODO
    }

    private void loadLatestVideoList() {
        // TODO
    }

    void slideUpIfNeeded() {
        if (mDraggableLayout.getVisibility() == View.VISIBLE) {
            mStatus = Status.HalfScreen;
            return;
        }
        mDraggableLayout.setVisibility(View.VISIBLE);
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mDraggableLayout, "translationY", SCREEN_SIZE_HEIGHT, 0);
        objectAnimator.setInterpolator(new AccelerateInterpolator());
        objectAnimator.setDuration(300);
        objectAnimator.addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                mStatus = Status.HalfScreen;
            }
        });
        objectAnimator.start();
    }

    void slideDown() {
        mStatus = Status.ToMain;
        mVideoPlayerView.pause();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mDraggableLayout, "translationY", 0, SCREEN_SIZE_HEIGHT);
        objectAnimator.setInterpolator(new AccelerateInterpolator());
        objectAnimator.setDuration(300);
        objectAnimator.addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                mStatus = Status.Main;
                mDraggableLayout.setVisibility(View.GONE);
            }
        });
        objectAnimator.start();
    }

}
