package com.ppippe.jibs.common;

import android.support.multidex.MultiDexApplication;

import com.google.firebase.analytics.FirebaseAnalytics;

public class JibsApplication extends MultiDexApplication {

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public FirebaseAnalytics getFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }
}
