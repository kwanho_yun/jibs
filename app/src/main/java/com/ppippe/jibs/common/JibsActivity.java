package com.ppippe.jibs.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.ppippe.jibs.util.Objects;

public class JibsActivity extends AppCompatActivity {
    protected JibsApplication getJibsApplication() {
        return (JibsApplication) super.getApplication();
    }

    protected FirebaseAnalytics getFirebaseAnalytics() {
        return getJibsApplication().getFirebaseAnalytics();
    }

    protected void logEvent(@NonNull String name, @NonNull String key, @NonNull String value) {
        Objects.throwIfNull(name);
        Objects.throwIfNull(key);
        Objects.throwIfNull(value);

        Bundle data = new Bundle();
        data.putString(key, value);
        getFirebaseAnalytics().logEvent(name, data);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logEvent(getClass().getSimpleName(), "event", "onCreate");
    }

    @Override
    protected void onResume() {
        super.onResume();
        logEvent(getClass().getSimpleName(), "event", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        logEvent(getClass().getSimpleName(), "event", "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logEvent(getClass().getSimpleName(), "event", "onDestroy");
    }
}
